WWU College Sites Installation Profile
================

This installation profile delivers a packaged set of modules, features, and a custom zen sub-theme for use with Western Washington University Drupal sites.

For a full listing of the modules, libraries, and themes we support please review the [make](https://bitbucket.org/wwuweb/collegesites/src/8fdc88827ad424aa5908b7815b39475175052222/collegesites.make?at=master) file.

Release 2.4.0
----------------

### Updates

* Drupal to 7.38
* Date iCal to 3.4
* jQuery Update to 2.6
* Media YouTube to 3.0
* Admin Views to 1.4

