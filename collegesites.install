<?php

/**
 * @file
 *
 * Install, update and uninstall functions for the WWU College Sites
 * installation profile.
 */

/**
 * Implements hook_install().
 *
 * - Run the hook_install() function from the minimal profile first.
 * - Run custom configuration functions as defined in this file.
 * - Rebuild node access permissions.
 */
function collegesites_install() {
  require_once DRUPAL_ROOT . '/profiles/minimal/minimal.install';
  minimal_install();

  _collegesites_configure_themes();
  _collegesites_configure_text_formats();
  _collegesites_configure_node_types();
  _collegesites_configure_rdf();
  _collegesites_configure_user_picture();
  _collegesites_configure_image_field();
  _collegesites_configure_administrator();
  _collegesites_configure_main_menu();
  _collegesites_configure_file_entity();
  _collegesites_configure_wysiwyg();
  _collegesites_configure_cas();

  node_access_rebuild();
}

/**
 * Create an administrator role and assign it to user 1.
 */
function _collegesites_configure_administrator() {
  $admin_role = new stdClass();
  $admin_role->name = 'administrator';
  $admin_role->weight = 2;

  user_role_save($admin_role);
  user_role_grant_permissions($admin_role->rid, array_keys(module_invoke_all('permission')));
  variable_set('user_admin_role', $admin_role->rid);
  db_insert('users_roles')
    ->fields(array('uid' => 1, 'rid' => $admin_role->rid))
    ->execute();
}

/**
 * Configure CAS server settings.
 */
function _collegesites_configure_cas() {
  variable_set('cas_server', 'websso.wwu.edu');
  variable_set('cas_uri', '/cas');
  variable_set('cas_login_invite', 'Editors and Administrators - Log in using Universal Login');
  variable_set('cas_login_drupal_invite', 'Cancel Universal Login');
  variable_set('cas_login_redir_message', 'You will be redirected to the secure Universal login page.');
  variable_set('cas_login_message', 'Logged in via Universal Login as %cas_username.');
  variable_set('cas_check_first', 1);
  variable_set('cas_pages', 'login');
  variable_set('cas_exclude', '*');
  variable_set('cas_first_login_destination', 'user');
  variable_set('cas_logout_destination', '<front>');
}

/**
 * Enable the default display for files.
 *
 * - Images display as images.
 * - Video uses oembed.
 */
function _collegesites_configure_file_entity() {
  $records = array(
    array(
      'name' => 'image__defualt__file_image',
      'status' => '1',
    ),
    array(
      'name' => 'video__default__file_field_file_video',
      'status' => '0',
    ),
    array(
      'name' => 'video__default__oembed',
      'status' => '1',
      'settings' => array('width' => '560', 'height' => '340', 'wmode' => '',),
    ),
  );

  foreach ($records as $record) {
    drupal_write_record('file_display', $record);
  }
}

/**
 * Create an image field and add it to the 'Basic Page' content type.
 * See:
 * - http://api.drupal.org/api/function/field_create_field/7
 */
function _collegesites_configure_image_field() {
  $field_image = array(
    'field_name' => 'field_image',
    'type' => 'image',
    'cardinality' => 1,
    'locked' => FALSE,
    'indexes' => array('fid' => array('fid')),
    'settings' => array(
      'uri_scheme' => 'public',
      'default_image' => FALSE,
    ),
    'storage' => array(
      'type' => 'field_sql_storage',
      'settings' => array(),
    ),
  );

  field_create_field($field_image);

  $field_image_instance = array(
    'field_name' => 'field_image',
    'entity_type' => 'node',
    'label' => 'Image',
    'bundle' => 'page',
    'description' => st('Upload an image to insert into the page.'),
    'required' => FALSE,
    'settings' => array(
      'file_directory' => 'field/image',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'alt_field' => TRUE,
      'title_field' => TRUE,
    ),
    'widget' => array(
      'type' => 'image_image',
      'settings' => array(
        'progress_indicator' => 'throbber',
        'preview_image_style' => 'thumbnail',
      ),
      'weight' => -1,
    ),
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'type' => 'image',
        'settings' => array('image_style' => 'large', 'image_link' => ''),
        'weight' => -1,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'type' => 'image',
        'settings' => array('image_style' => 'medium', 'image_link' => 'content'),
        'weight' => -1,
      ),
    ),
  );

  field_create_instance($field_image_instance);
}

/**
 * Add a link to the home page on the main menu.
 */
function _collegesites_configure_main_menu() {
  $item = array(
    'link_title' => st('Home'),
    'link_path' => '<front>',
    'menu_name' => 'main-menu',
  );

  menu_link_save($item);
  menu_rebuild();
}

/**
 * Configure the default nodes types 'Article' and 'Basic Page'.
 *
 * - Turn off comment and author display on those node types.
 * See:
 * - Node Type API: http://api.drupal.org/api/HEAD/function/hook_node_info
 */
function _collegesites_configure_node_types() {
  $node_types = array(
    array(
      'type' => 'page',
      'name' => st('Basic page'),
      'base' => 'node_content',
      'description' => st("Use <em>basic pages</em> for your static content, such as an 'About us' page."),
      'custom' => 1,
      'modified' => 1,
      'locked' => 0,
    ),
    array(
      'type' => 'article',
      'name' => st('Article'),
      'base' => 'node_content',
      'description' => st('Use <em>articles</em> for time-sensitive content like news, press releases or blog posts.'),
      'custom' => 1,
      'modified' => 1,
      'locked' => 0,
    ),
  );

  foreach ($node_types as $type) {
    $type = node_type_set_defaults($type);
    node_type_save($type);
    node_add_body_field($type);
  }

  variable_set('node_options_page', array('status'));
  variable_set('comment_page', COMMENT_NODE_HIDDEN);
  variable_set('node_submitted_page', FALSE);
}

/**
 * Save default RDF settins to the database.
 */
function _collegesites_configure_rdf() {
  $rdf_mappings = array(
    array(
      'type' => 'node',
      'bundle' => 'page',
      'mapping' => array(
        'rdftype' => array('foaf:Document'),
      ),
    ),
    array(
      'type' => 'node',
      'bundle' => 'article',
      'mapping' => array(
        'field_image' => array(
          'predicates' => array('og:image', 'rdfs:seeAlso'),
          'type' => 'rel',
        ),
        'field_tags' => array(
          'predicates' => array('dc:subject'),
          'type' => 'rel',
        ),
      ),
    ),
  );

  foreach ($rdf_mappings as $rdf_mapping) {
    rdf_mapping_save($rdf_mapping);
  }
}

/**
 * Configure the 'Clean HTML' and 'Full HTML' text formats. Text format
 * information is stored in the filter database table.
 */
function _collegesites_configure_text_formats() {
  $clean_html = array(
    'format' => 'clean_html',
    'name' => 'Clean HTML',
    'weight' => 0,
    'filters' => array(
      'image_resize_filter' => array(
        'weight' => -45,
        'status' => 1,
      ),
      'media_filter' => array(
        'weight' => -46,
        'status' => 1,
      ),
      'html_purifier_advanced' => array(
        'weight' => -44,
        'status' => 1,
      ),
      'oembed' => array(
        'weight' => -43,
        'status' => 1,
      ),
    ),
  );

  $full_html = array(
    'format' => 'full_html',
    'name' => 'Full HTML',
    'weight' => 1,
    'filters' => array(
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
      ),
      'image_resize_filter' => array(
        'weight' => 0,
        'status' => 1,
      ),
      'filter_autop' => array(
        'weight' => 1,
        'status' => 1,
      ),
      'media_filter' => array(
        'weight' => 2,
        'status' => 1,
      ),
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
      ),
    ),
  );

  foreach (array($clean_html, $full_html) as $format) {
    filter_format_save((object) $format);
  }
}

/**
 * Disable all default themes and enable 'wwuzen' and 'wwuadminimal' themes.
 */
function _collegesites_configure_themes() {
  db_update ('system')
    ->fields(array('status' => 0))
    ->condition('type', 'theme')
    ->execute();

  $admin = 'wwuadminimal';

  db_update('system')
    ->fields(array('status' => 1))
    ->condition('type', 'theme')
    ->condition('name', $admin)
    ->execute();
  variable_set('admin_theme', $admin);
  variable_set('node_admin_theme', '1');

  $default = 'wwuzen';

  db_update('system')
    ->fields(array('status' => 1))
    ->condition('type', 'theme')
    ->condition('name', $default)
    ->execute();
  variable_set('theme_default', $default);
}

/**
 * Enable user image upload, set the default display to thumbnail. Maximum size
 * is set to 1024x1024 pixels.
 */
function _collegesites_configure_user_picture() {
  variable_set('user_pictures', '1');
  variable_set('user_picture_dimensions', '1024x1024');
  variable_set('user_picture_file_size', '800');
  variable_set('user_picture_style', 'thumbnail');
}

/**
 * Configure the 'Clean HTML' and 'Full HTML' WYSIWYG profiles. Clean HTML is
 * use as the default, Full HTML settings are merged in using array_replace().
 * See:
 * - Linux Journal Drupal special edition, page 96
 * - https://www.drupal.org/project/media_dev
 */
function _collegesites_configure_wysiwyg() {
  $buttons = array(
    'default' => array(
      'Bold' => 1,
      'Italic' => 1,
      'Underline' => 1,
      'Strike' => 1,
      'JustifyLeft' => 1,
      'JustifyCenter' => 1,
      'JustifyRight' => 1,
      'JustifyBlock' => 1,
      'BulletedList' => 1,
      'NumberedList' => 1,
      'Outdent' => 1,
      'Indent' => 1,
      'Undo' => 1,
      'Redo' => 1,
      'Link' => 1,
      'Unlink' => 1,
      'Anchor' => 1,
      'Image' => 1,
      'Superscript' => 1,
      'Subscript' => 1,
      'Blockquote' => 1,
      'Cut' => 1,
      'Copy' => 1,
      'PasteText' => 1,
      'PasteFromWord' => 1,
      'SpecialChar' => 1,
      'Format' => 1,
      'FontSize' => 1,
      'Styles' => 1,
      'Table' => 1,
      'Find' => 1,
      'Replace' => 1,
      'SpellChecker' => 1,
      'Scayt' => 1,
      'Source' => 1,
    ),
    'drupal' => array(
      'media' => 1,
    ),
  );

  $clean_html = array(
    'default' => 1,
    'user_choose' => 0,
    'show_toggle' => 0,
    'theme' => 'advanced',
    'language' => 'en',
    'buttons' => $buttons,
    'toolbarLocation' => 'top',
    'toolbar_align' => 'left',
    'resize_enabled' => 1,
    'preformatted' => 0,
    'convert_fonts_to_spans' => 1,
    'remove_linebreaks' => 1,
    'simple_source_formatting' => 1,
    'acf_mode' => 1,
    'acf_allowed_content' =>'iframe[*];img[typeof,data-fid,data-media-element];mediawrapper[!data];*[!data-file_info]',
    'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
    'css_setting' => 'self',
    'css_path' => '/sites/all/themes/wwuzen/css/pages.css',
    'css_classes' => '',
  );

  $full_html = array_replace($clean_html, array(
    'verify_html' => 1,
    'convert_fonts_to_spans' => 0,
    'paste_auto_cleanup_on_paste' => 1,
    'apply_source_formatting' => 1,
    'simple_source_formatting' => 0,
    'acf_mode' => 0,
    'acf_allowed_content' => '',
  ));

  $records = array(
    array(
      'format' => 'clean_html',
      'editor' => 'ckeditor',
      'settings' => $clean_html,
    ),
    array(
      'format' => 'full_html',
      'editor' => 'ckeditor',
      'settings' => $full_html,
    ),
  );

  foreach ($records as $record) {
    drupal_write_record('wysiwyg', $record);
  }
}
