; ----------------
; Generated makefile from http://drushmake.me
; Permanent URL: http://drushmake.me/file.php?token=0197d4ae2cbd
; ----------------
;
; This is a working makefile - try it! Any line starting with a `;` is a comment.

; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.

core = 7.x

; API version
; ------------
; Every makefile needs to declare its Drush Make API version. This version of
; drush make uses API version `2`.

api = 2

; Core project
; ------------
; In order for your makefile to generate a full Drupal site, you must include
; a core project. This is usually Drupal core, but you can also specify
; alternative core projects like Pressflow. Note that makefiles included with
; install profiles *should not* include a core project.

; Drupal 7.x. Requires the `core` property to be set to 7.x.
projects[drupal][version] = "7.41"

; Base Modules
; --------

projects[accordion_menu][version] = 1.2
projects[accordion_menu][type]= "module"
projects[accordion_menu][patch][] = "https://www.drupal.org/files/issues/accordion_menu-jquery-ui-1x10-support-2190307-1.patch"

projects[addressfield][version] = 1.2
projects[addressfield][type]= "module"

projects[adminimal_admin_menu][version] = 1.5
projects[adminimal_admin_menu][type] = "module"

projects[advanced_help][version] = 1.1
projects[advanced_help][type] = "module"

projects[auto_entitylabel][version] = 1.3
projects[auto_entitylabel][type]= "module"

projects[cas][version] = 1.4
projects[cas][type] = "module"

projects[calendar][version] = 3.5
projects[calendar][type] = "module"

projects[ctools][version] = 1.9
projects[ctools][type] = "module"
projects[ctools][patch][] = "https://www.drupal.org/files/ctools-1405988.patch"

projects[date][version] = 2.8
projects[date][type] = "module"
projects[date][patch][] = "https://www.drupal.org/files/d7-1053158-date-additional-2.patch"

projects[date_ical][version] = 3.4
projects[date_ical][type] = "module"

projects[diff][version] = 3.2
projects[diff][type] = "module"

projects[entity][version] = 1.6
projects[entity][type] = "module"

projects[entityreference][version] = 1.1
projects[entityreference][type] = "module"

projects[email][version] = 1.3
projects[email][type] = "module"

projects[feeds][version] = 2.0-alpha9
projects[feeds][type] = "module"

projects[fences][version] = 1.0
projects[fences][type] = "module"

projects[file_entity][version] = 2.x-dev
projects[file_entity][type] = "module"
projects[file_entity][download][type] = git
projects[file_entity][download][revision] = 319f22a
projects[file_entity][patch][] = "https://www.drupal.org/files/issues/file_entity-addcheckforemptyfiles-2443059-2.patch"

projects[flexifilter][version] = 1.x-dev
projects[flexifilter][type] = "module"

projects[flexslider][version] = 2.0-alpha3
projects[flexslider][type] = "module"

projects[google_appliance][version] = 1.13
projects[google_appliance][type] = "module"

projects[hierarchical_select][version] = 3.0-alpha13
projects[hierarchical_select][type]= "module"

projects[htmlpurifier][version] = 1.0
projects[htmlpurifier][type] = "module"

projects[image_resize_filter][version] = 1.14
projects[image_resize_filter][type]= "module"

projects[imce][version] = 1.9
projects[imce][type] = "module"

projects[imce_wysiwyg][version] = 1.0
projects[imce_wysiwyg][type] = "module"

projects[insert][version] = 1.3
projects[insert][type]= "module"

projects[jquery_update][version] = 2.7
projects[jquery_update][type]= "module"

projects[job_scheduler][version] = 2.0-alpha3
projects[job_scheduler][type]= "module"

projects[link][version] = 1.3
projects[link][type] = "module"

projects[linkchecker][version] = 1.2
projects[linkchecker][type]= "module"

projects[linkit][version] = 3.3
projects[linkit][type] = "module"

projects[media][version] = 2.x-dev
projects[media][type]= "module"
projects[media][download][type] = git
projects[media][download][revision] = 15a3657
projects[media][patch][] = "https://www.drupal.org/files/issues/file_entity-remove-contextual-links-2401811-11.patch"
projects[media][patch][] = "https://bitbucket.org/wwuweb/wwu-media-update-hook-patch/raw/3cf2dd28e58e61ffeeac7840fe6d0c703873b89f/media-missing_update_hook.patch"

libraries[wwu_mediafix][download][type] = "git"
libraries[wwu_mediafix][download][url] = "https://bitbucket.org/wwuweb/wwu_mediafix.git"
libraries[wwu_mediafix][destination] = "modules"

projects[media_youtube][version] = 3.0
projects[media_youtube][type] = "module"

projects[media_vimeo][version] = 2.0
projects[media_vimeo][type] = "module"

projects[oauth][version] = 3.2
projects[oauth][type] = "module"

projects[oembed][version] = 1.x-dev
projects[oembed][type] = "module"
projects[oembed][download][type] = git
projects[oembed][download][revision] = 1664b19


projects[og][version] = 2.7
projects[og][type] = "module"
projects[og][patch][] = "https://www.drupal.org/files/issues/group_reference_token-2264759-17.patch"

;projects[og_vocab][version] = 1.1
;projects[og_vocab][type] = "module"

projects[pathauto][version] = 1.3
projects[pathauto][type] = "module"

projects[pathologic][version] = 2.12
projects[pathologic][type] = "module"

projects[panels][version] = 3.5
projects[panels][type]= "module"

projects[phone][version] = 1.0-beta1
projects[phone][type]= "module"

projects[token][version] = 1.5
projects[token][type] = "module"

projects[wysiwyg][version] = 2.x-dev
projects[wysiwyg][type] = "module"
projects[wysiwyg][download][type] = "git"
projects[wysiwyg][download][revision] = 898d02

projects[views][version] = 3.11
projects[views][type] = "module"

projects[views_bulk_operations][version] = 3.3
projects[views_bulk_operations][type] = "module"

projects[wwuzen_gsa][download][type] = "git"
projects[wwuzen_gsa][download][url] = "https://bitbucket.org/wwuweb/wwu-google-search-appliance.git"
projects[wwuzen_gsa][type] = "module"


; Ancilliary Modules
; ------------------

projects[admin_views][version]= 1.5
projects[admin_views][type] = "module"

projects[addanother][version] = 2.2
projects[addanother][type] = "module"

projects[admin_menu][version] = 3.0-rc5
projects[admin_menu][type] = "module"

projects[backup_migrate][version] = 3.0
projects[backup_migrate][type] = "module"

projects[clientside_validation][version] = 1.41
projects[clientside_validation][type] = "module"

projects[colorbox][version] = 2.10
projects[colorbox][type] = "module"

projects[clean_markup][version] = 2.7
projects[clean_markup][type] = "module"

projects[conditional_fields][version] = 3.0-alpha1
projects[conditional_fields][type] = "module"

projects[draggableviews][version] = 2.0
projects[draggableviews][type] = "module"

projects[edit_profile][version] = 1.0-beta2
projects[edit_profile][type] = "module"

projects[elements][version] = 1.4
projects[elements][type] = "module"

projects[entity_view_mode][version] = 1.0-rc1
projects[entity_view_mode][type] = "module"

projects[environment_indicator][version] = 2.7
projects[environment_indicator][type] = "module"

projects[exclude_node_title][version] = 1.7
projects[exclude_node_title][type] = "module"

projects[facebook_pull][version] = 2.4
projects[facebook_pull][type] = "module"

projects[features][version] = 2.3
projects[features][type] = "module"

projects[feeds_tamper] = 1.1
projects[feeds_tamper][type] = "module"

projects[field_collection][version] = 1.0-beta8
projects[field_collection][type] = "module"
projects[field_collection][patch][] = "https://www.drupal.org/files/field_collection-feeds_integration.patch"

projects[field_group][version] = 1.4
projects[field_group][type] = "module"

projects[focal_point][version] = 1.0-beta1
projects[focal_point][type] = "module"

projects[form_builder][version] = 1.13
projects[form_builder][type] = "module"

projects[globalredirect][version] = 1.5
projects[globalredirect][type] = "module"

projects[google_analytics][version] = 2.1
projects[google_analytics][type] = "module"

projects[google_appliance_suggest][version] = 1.4
projects[google_appliance_suggest][type] = "module"

projects[wwu_greek][download][type] = "git"
projects[wwu_greek][download][url] = "https://bitbucket.org/wwuweb/wwu_greek.git"
projects[wwu_greek][type] = "module"

projects[wwu_scayt_extension][download][type] = "git"
projects[wwu_scayt_extension][download][url] = "https://bitbucket.org/wwuweb/wwu_scayt_extension.git"
projects[wwu_scayt_extension][type] = "module"

projects[hidden_nodes][version] = 1.4
projects[hidden_nodes][type] = "module"

projects[ldap][version] = 2.0-beta8
projects[ldap][type] = "module"

projects[libraries][version] = 2.2
projects[libraries][type] = "module"

projects[mailchimp][version] = 3.3
projects[mailchimp][type] = "module"

projects[mailsystem][version] = 2.34
projects[mailsystem][type] = "module"

projects[media_browser_plus][version] = 3.0-beta3
projects[media_browser_plus][type] = "module"

projects[menu_admin_per_menu][version] = 1.0
projects[menu_admin_per_menu][type] = "module"

projects[mimemail][version] = 1.0-beta3
projects[mimemail][type] = "module"

projects[module_filter][version] = 2.0-alpha2
projects[module_filter][type] = "module"

projects[mollom][version] = 2.13
projects[mollom][type] = "module"

projects[menu_attributes][version] = 1.0-rc3
projects[menu_attributes][type] = "module"

projects[menu_block][version] = 2.3
projects[menu_block][type] = "module"
projects[menu_block][patch][] = "http://drupalcode.org/project/accordion_menu.git/blob_plain/1647e2693b347e9a3bd68b5609ad87d535d59149:/menu_block-7.x-2.3.patch"
projects[menu_block][patch][] = "https://drupal.org/files/menu_block-7.x-2.3-plugin.patch"

projects[multiform][version] = 1.1
projects[multiform][type] = "module"

projects[multiupload_filefield_widget][version] = 1.13
projects[multiupload_filefield_widget][type] = "module"

projects[multiupload_imagefield_widget][version] = 1.3
projects[multiupload_imagefield_widget][type] = "module"

projects[node_clone][version] = 1.0-rc2
projects[node_clone][type] = "module"

projects[node_embed][version] = 1.1
projects[node_embed][type] = "module"

projects[nodequeue][version] = 2.0-beta1
projects[nodequeue][type] = "module"

projects[og_menu][version] = 3.0
projects[og_menu][type] = "module"

projects[og_webform][version] = 1.0-beta1
projects[og_webform][type] = "module"
projects[og_webform][patch][] = "http://cgit.drupalcode.org/og_webform/patch/?id=b60f03ae4de8050bb2499106484df085b9884b25"
projects[og_webform][patch][] = "https://www.drupal.org/files/og_webform_api2-1946432_0.patch"

projects[options_element][version] = 1.12
projects[options_element][type] = "module"

projects[plupload][version] = 1.7
projects[plupload][type] = "module"

projects[quicktabs][version] = 3.6
projects[quicktabs][type] = "module"

projects[rules][version] = 2.8
projects[rules][type] = "module"

projects[r4032login][version] = 1.8
projects[r4032login][type] = "module"

projects[select_or_other][version] = 2.22
projects[select_or_other][type] = "module"

projects[simplify][version] = 3.2
projects[simplify][type] = "module"

projects[taxonomy_csv][version] = 5.10
projects[taxonomy_csv][type] = "module"

projects[themekey][version] = 3.2
projects[themekey][type] = "module"

projects[token_filter][version] = 1.1
projects[token_filter][type] = "module"

projects[twitter][version] = 5.10
projects[twitter][type] = "module"
projects[twitter][subdir] = contrib

projects[views_accordion][version] = 1.1
projects[views_accordion][type] = "module"

projects[viewfield][version] = 2.0
projects[viewfield][type] = "module"

projects[views_field_view][version] = 1.1
projects[views_field_view][type] = "module"

projects[views_responsive_grid][version] = 1.3
projects[views_responsive_grid][type] = "module"

projects[views_tree][version] = 2.x-dev
projects[views_tree][type] = "module"
projects[views_tree][download][type] = git
projects[views_tree][download][revision] = 7dd712

projects[webform][version] = 4.9
projects[webform][type] = "module"

projects[webform_scheduler][version] = 1.0-beta8
projects[webform_scheduler][type] = "module"

projects[webform_validation][version] = 1.10
projects[webform_validation][type] = "module"

projects[webform_report][version] = 1.x-dev
projects[webform_report][type] = "module"
projects[webform_report][download][type] = git
projects[webform_report][download][revision] = 04cbfc

projects[webform_rules][version] = 1.6
projects[webform_rules][type] = "module"
projects[webform_rules][patch][] = "https://www.drupal.org/files/issues/webform_rules-expose_submission_data-2020149-56.patch"
projects[webform_rules][patch][] = "https://www.drupal.org/files/issues/webform_rules-expose_submission_data-2020149-27.patch"
projects[webform_rules][patch][] = "https://www.drupal.org/files/issues/webform_rules-fix_for_conditional_fail-2554435-5.patch"

projects[weight][version] = 2.4
projects[weight][type] = "module"

projects[workbench][version] = 1.2
projects[workbench][type]= "module"

projects[workbench_access][version] = 1.2
projects[workbench_access][type]= "module"

projects[workbench_email][version] = 3.4
projects[workbench_email][type] = "module"

projects[workbench_media][version] = 2.1
projects[workbench_media][type]= "module"

projects[workbench_moderation][version] = 1.3
projects[workbench_moderation][type]= "module"
projects[workbench_moderation][patch][] = "https://www.drupal.org/files/issues/2148021-multiple_moderation_status-2.patch"

projects[xmlsitemap][version] = 2.2
projects[xmlsitemap][type] = "module"

projects[wwu_img_caption][download][type] = "git"
projects[wwu_img_caption][download][url] = "https://bitbucket.org/wwuweb/wwu_img_caption.git"
projects[wwu_img_caption][type] = "module"

projects[wysiwyg_tools_plus][version] = 1.0-alpha2
projects[wysiwyg_tools_plus][type] = "module"
projects[wysiwyg_tools_plus][patch][] = "https://www.drupal.org/files/issues/wysiwyg_tools_plus-css-path-fix.patch"

; Themes
; --------

projects[adminimal_theme][version] = 1.20
projects[adminimal_theme][type] = "theme"

projects[zen][version] = 5.5
projects[zen][type] = "theme"

projects[wwuzen][download][type] = "git"
projects[wwuzen][download][url] = "https://bitbucket.org/wwuweb/wwuzen.git"
projects[wwuzen][type] = "theme"

projects[wwuadminimal][download][type] = "git"
projects[wwuadminimal][download][url] = "https://bitbucket.org/wwuweb/wwuadminimal.git"
projects[wwuadminimal][type] = "theme"

; Libraries
; ---------

;The cas module requires the library directory to be all uppercase
libraries[CAS][download][type] = "file"
libraries[CAS][download][url] = "http://downloads.jasig.org/cas-clients/php/current/CAS-1.3.4.tgz"
libraries[CAS][download][subtree] = CAS-1.3.4/

; If/when we upgrade to CKEditor 4.5, revisit ticket CDS-884. I.e. see if turning off image_prefillDimensions
; will prevent the editor from inserting inline styles. If that is the case, remove the !important flags
; from the responsive image styles in normalize.scss.

libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.6/ckeditor_4.4.6_full.zip"

libraries[colorbox][download][type] = "file"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/master.zip"

libraries[flexslider][download][type] = "file"
libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider/archive/version/2.2.2.zip"

libraries[jquery][download][type] = "file"
libraries[jquery][download][url] = "https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"

libraries[jqueryui][download][type] = "file"
libraries[jqueryui][download][url] = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"

libraries[mailchimp][download][type] = "get"
libraries[mailchimp][download][url] = "https://bitbucket.org/mailchimp/mailchimp-api-php/get/2.0.4.zip"
libraries[mailchimp][directory_name] = "mailchimp"
libraries[mailchimp][destination] = "libraries"

;For use by feeds module
;libraries[simplepie][download][type] = "file"
;libraries[simplepie][download][url] = "https://github.com/simplepie/simplepie/archive/1.3.1.tar.gz"
;libraries[simplepie][download][subtree] = /library
;libraries[simplepie][destination] = "sites/all/modules/feeds/libraries"

libraries[simple_html_dom][download][type] = "file"
libraries[simple_html_dom][download][url] = "http://iweb.dl.sourceforge.net/project/simplehtmldom/simplehtmldom/1.5/simplehtmldom_1_5.zip"


; Features
; --------

libraries[default_roles_permissions][download][type] = "git"
libraries[default_roles_permissions][download][url] = "https://bitbucket.org/wwuweb/default_roles_permissions.git"
libraries[default_roles_permissions][destination] = "modules"

libraries[gallery][download][type] = "git"
libraries[gallery][download][url] = "https://bitbucket.org/wwuweb/gallery.git"
libraries[gallery][destination] = "modules"

libraries[organizational_unit][download][type] = "git"
libraries[organizational_unit][download][url] = "https://bitbucket.org/wwuweb/organizational_unit.git"
libraries[organizational_unit][destination] = "modules"

libraries[slideshow][download][type] = "git"
libraries[slideshow][download][url] = "https://bitbucket.org/wwuweb/slideshow.git"
libraries[slideshow][destination] = "modules"

libraries[staff_and_faculty_profiles_and_directory][download][type] = "git"
libraries[staff_and_faculty_profiles_and_directory][download][url] = "https://bitbucket.org/wwuweb/staff_and_faculty_profiles_and_directory.git"
libraries[staff_and_faculty_profiles_and_directory][destination] = "modules"

libraries[wwu_faq][download][type] = "git"
libraries[wwu_faq][download][url] = "https://bitbucket.org/wwuweb/wwu_faq.git"
libraries[wwu_faq][destination] = "modules"

libraries[wwu_fields][download][type] = "git"
libraries[wwu_fields][download][url] = "https://bitbucket.org/wwuweb/wwu_fields.git"
libraries[wwu_fields][destination] = "modules"

libraries[wwu_student_profiles][download][type] = "git"
libraries[wwu_student_profiles][download][url] = "https://bitbucket.org/wwuweb/wwu-student-profiles.git"
libraries[wwu_student_profiles][destination] = "modules"
